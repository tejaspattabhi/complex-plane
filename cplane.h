#include <stdio.h>
#include <stdlib.h>
#include "complex.h"

#ifndef CPLANE_H_
#define CPLANE_H_

#define SIZE 100

struct CPlane {
	long double xmin, xmax, ymin, ymax;
	unsigned long int xpoints, ypoints;
	
	COMPLEX ** mat;
};

typedef struct CPlane CPLANE;

// Constructors
CPLANE newCPlane (long double, long double, long double, long double, unsigned long int, unsigned long int);

#endif
