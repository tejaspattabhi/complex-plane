#include <stdio.h>
#include <stdlib.h>

struct MATRIX
{
        unsigned int rows;
        unsigned int cols;
        long double *mat;

};

typedef struct MATRIX aMatrix;

// Construct a matrix that allows us set and populate our dynamic array
void set(aMatrix *m, int i, int j, long double value){
        *(m-> mat + m->rows*i + j) = value;
}

// get the value at a location i,j

long double get(aMatrix *m, int i, int j){
        return *(m-> mat + m-> rows*i + j);
}

aMatrix new(unsigned int rows, unsigned int cols){

        aMatrix A;
        A.rows = rows;
        A.cols = cols;
        A.mat = (long double *) calloc(A.rows*A.cols, sizeof(long double));

        return A;
}
//=================================================================================
// you need to create the print matrix here.
//========================================================================
aMatrix  matprod(aMatrix firstmat, aMatrix secondmat){
        if(firstmat.cols == secondmat.rows){
                aMatrix C = new(firstmat.rows, secondmat.cols);
                int i,j,k;
                for( i = 0; i < firstmat.rows ; i++){
                        for(j = 0; j < secondmat.cols; j++){
                                long double  thesum = 0;
                                for(k = 0; k < secondmat.rows ; k++){
                                        thesum = thesum + get(&firstmat,i,k)*get(&secondmat,k,j);
                                }
                                set(&C, i, j, thesum);
                        }
                }

                return C;
        }
                                                                                                                                                                                             