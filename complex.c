#include <stdio.h>
#include <stdlib.h>
#include "cplane.h"

COMPLEX newComplex (long double real, long double imaginary) {
	COMPLEX temp;
	
	temp.real = real;
	temp.imaginary = imaginary;
	
	// printf ("(%Lf + i %Lf)", real, imaginary);
	
	return temp;
}