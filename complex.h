#include <stdio.h>
#include <stdlib.h>

#ifndef COMPLEX_H_
#define COMPLEX_H_

struct Complex {
	long double real;
	long double imaginary;
};

typedef struct Complex COMPLEX;

COMPLEX newComplex (long double, long double);

#endif