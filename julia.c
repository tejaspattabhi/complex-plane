#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "complex.h"
#include "cplane.h"

#define MAXITER 256

// Julia-map is also a hunch
int juliamap (COMPLEX z, COMPLEX c) {
	COMPLEX temp;
	long double magnitude;
	
	// Assuming that "the complex result" in the question means sum of 2 complex numbers,
	magnitude = 0.0;
	
	// Find the sum
	temp.real = z.real + c.real;
	temp.imaginary = z.imaginary + c.imaginary;
	
	// Find Magnitude of the sum	
	magnitude = sqrt ((double)(temp.real * temp.real + temp.imaginary * temp.imaginary));
	
	printf ("Magnitude: %Lf\n", magnitude);
	
	return magnitude;
}

int main (int argc, char ** argv) {
	long double xmin, xmax, ymin, ymax, real, imaginary, magnitude;
	unsigned long int xpoints, ypoints;
	int i, j, iCount;
	
	CPLANE plane;
	COMPLEX c;
	
	if (argc != 9) {
		printf ("Usage: %s XMAX XMIN YMAX YMIN XPOINTS YPOINTS CREAL CIMAGINARY\n", argv[0]);
		return 1;
	}
	
	iCount = 0;
	magnitude = 0.0;
	
	xmin = (long double) atof (argv[1]);
	xmax = (long double) atof (argv[2]);
	ymin = (long double) atof (argv[3]);
	ymax = (long double) atof (argv[4]);
	xpoints = (unsigned long int) atol (argv[5]);
	ypoints = (unsigned long int) atol (argv[6]);
	real = (long double) atof (argv[7]);
	imaginary = (long double) atof (argv[8]);
	
	plane = newCPlane (xmin, xmax, ymin, ymax, xpoints, ypoints);
	c = newComplex (real, imaginary);
	
	// From here, whatever I am doing is just a hunch!! So, be careful
	for (i = 0; i < plane.xpoints; i++) {
		for (j = 0; j < plane.ypoints; j++) {
			// Iteration Count
			iCount++;
			
			magnitude = juliamap (plane.mat[i][j], c);
			if (magnitude > 2.0) {
				printf ("%Lf,%Lf,%Lf\n", plane.mat[i][j].real, plane.mat[i][j].imaginary, magnitude);
				printf ("Iterations needed: %d\n", iCount);
				return iCount;
			}
			
			if (iCount > MAXITER) {
				printf ("Iterations crossed: %d\n", iCount);
				return 0;
			}
		}
	}
	
	return 0;
}