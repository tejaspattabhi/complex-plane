#include <stdio.h>
#include <stdlib.h>
#include "cplane.h"
#include "complex.h"

CPLANE newCPlane (long double XMin, long double XMax, long double YMin, 
	long double YMax, unsigned long int XPoints, unsigned long int YPoints) {
		
		CPLANE temp;
		long double xinterval;
		long double yinterval;
		int i, j;
		
		xinterval = yinterval = 0.0;
		
		temp.xmin = XMin;
		temp.xmax = XMax;
		temp.ymin = YMin;
		temp.ymax = YMax;
		temp.xpoints = XPoints;
		temp.ypoints = YPoints;
		
		xinterval = (long double) ((XMax - XMin) / XPoints);
		yinterval = (long double) ((YMax - YMin) / YPoints);
		
		// allocate memory
		temp.mat = (COMPLEX**) malloc (YPoints * sizeof (COMPLEX *));
		for (i = 0; i < YPoints; i++) 
			temp.mat[i] = (COMPLEX *) malloc (XPoints * sizeof (COMPLEX));
		
		// printf ("(%Lf, %Lf)", xinterval, yinterval);
		
		// To comment
		// printf ("Matrix Building: \n");
		
		for (i = 0; i < YPoints; i++) {
			for (j = 0; j < XPoints; j++) {
				temp.mat[i][j] = newComplex ((XMin + j * xinterval), (YMin + i * yinterval));
				// printf ("\t");
			}
			// printf ("\n");
		}
		
		return temp;
}